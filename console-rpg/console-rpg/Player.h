#pragma once
#include <string>
#include <vector>
#include <iostream>
#include "Board.h"

class Player
{
public:
	Player();
	~Player();
	//setters
	void setName(std::string name);
	void improveAttackPower(int attackPower);
	void improveArmor(int armor);
	void changeHP(int hp);
	void gainExp(int exp);
	void setXandY(int x, int y);
	//getters
	int getAttackPower() {
		return _attackPower;
	};
	int getHp() {
		return _hp;
	};
	std::string getName() const {
		return _name;
	};
	int getExp() {
		return _exp;
	};
	int getLvl() {
		return _lvl;
	};
	int getPosX() {
		return _playerPosX;
	};
	int getPosY() {
		return _playerPosY;
	};
	int getArmor() {
		return _armor;
	}
	bool isAlive() {
		if (_hp > 0) return true;
		else return false;
	};

	friend std::ostream& operator<<(std::ostream& os, Player const& player);

private:
	std::string _name;
	int _lvl;
	int _exp;
	int _hp;
	int _attackPower;
	int _armor;

	int _playerPosX;
	int _playerPosY;

	void _lvlUp();
};

static std::ostream& operator<<(std::ostream& stream, Player const& player) {
	stream << "Hello " << player.getName() << "!\n";
	return stream;
}