#include "Board.h"
#include <fstream>
#include <iostream>

Board::Board() {
}


Board::~Board() {
}

void Board::loadBoard() { // load level from file
	std::ifstream myFile;
	myFile.open("board.txt");

	std::string line;
	if (myFile.is_open()) {
		while (std::getline(myFile, line)) {
			line = line + "\n";
			_board.push_back(line);
		}
	}

}

void Board::displayBoard() { // display board on screen
	for (int n = 0; n<_board.size(); n++) {
		std::cout << _board.at(n);
	}
}

void Board::checkBoard(char condition, int& x, int& y) { // check the position of player, monster or something
	int i = 0, j;
	for (j = 0; j < _board.size(); j++) {
		while (_board[j][i] != '\n') {
			if (_board[j][i] == condition) {
				x = j;
				y = i;
				return;
			}
			i++;
		}
		i = 0;
	}
}

void Board::checkBoard(std::vector<Zombie>& zombie, std::vector<Skeleton>& skeleton, int x, int y) { // adding monsters 
	int i = 0, j;
	for (j = 0; j < _board.size(); j++) {
		while (_board[j][i] != '\n') {
			if (_board[j][i] == 'Z') {
				x = j;
				y = i;
				zombie.push_back(std::move((Zombie(x, y))));
			}
			else if (_board[j][i] == 'S') {
				x = j;
				y = i;
				skeleton.push_back(std::move(Skeleton(x, y)));
			}
			i++;
		}
		i = 0;
	}
}

int Board::checkBoard(int x, int y) { // check what is in this place	
	if (_board[x][y] == ' ')
		return 1;

	else if (_board[x][y] == 'S' || _board[x][y] == 'Z')
		return 2;

	else if (_board[x][y] == 'B')
		return 3;

	return 0;
}

void Board::modifyBoard(int x1, int y1, int x2, int y2) { // implemented moving 
	char temp;

	temp = _board[x1][y1];
	_board[x1][y1] = _board[x2][y2];
	_board[x2][y2] = temp;
}

void Board::modifyBoard(int x1, int y1) { // implemented moving 
	_board[x1][y1] = ' ';
}