#include "Skeleton.h"



Skeleton::Skeleton(int x, int y) {
	_name = "Skeleton";
	_monsterPosX = x;
	_monsterPosY = y;
	_attackPower = 22;
	_armor = 10;
	_hp = 30;
}


Skeleton::~Skeleton() {
}

void Skeleton::fight(int& damage, int playerAttackPower, int playerArmor) {
	std::random_device r;

	std::default_random_engine e1(r());
	std::uniform_int_distribution<int> uniform_dist(1, 100);
	int attackChance = uniform_dist(e1);

	if (attackChance > 40) {
		damage += _attackPower - playerArmor;
	}

	_hp -= playerAttackPower - _armor;
}