#pragma once
#include "Monster.h"
#include <string>
#include <random>
class Zombie :
	public Monster
{
public:
	Zombie(int x, int y);
	Zombie();
	~Zombie();
	virtual void fight(int& damage, int playerAttackPower, int playerArmor);
private:

};

