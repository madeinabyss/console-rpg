#pragma once
#include <string>
#include <time.h>

class Monster
{
public:
	friend class Game;
	virtual void fight(int& damage, int playerAttackPower, int playerArmor) = 0;
	void setXandY(int x, int y) {
		_monsterPosX = x;
		_monsterPosY = y;
	}

protected:
	std::string _name;
	int _hp;
	int _attackPower;
	int _armor;

	int _monsterPosX;
	int _monsterPosY;
};

