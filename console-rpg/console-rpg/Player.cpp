#include "Player.h"


Player::Player() :
	_name("Gracz"),
	_lvl(1),
	_exp(0),
	_hp(100),
	_attackPower(22),
	_armor(10),
	_playerPosX(0),
	_playerPosY(0) {
}


Player::~Player() {
}

void Player::setName(std::string name) {
	_name = name;
}

void Player::improveAttackPower(int attackPower) {
	_attackPower += attackPower;
}

void Player::changeHP(int hp) {
	_hp += hp;
}

void Player::improveArmor(int armor) {
	_armor += armor;
}

void Player::gainExp(int exp) {
	_exp += exp;

	if (_exp >= _lvl * 173) {
		_lvlUp();
	}
}

void Player::setXandY(int x, int y) {
	_playerPosX = x;
	_playerPosY = y;
}

void Player::_lvlUp() {
	_exp = _exp - _lvl * 173;
	_lvl++;
	_attackPower += 2;
	_armor += 1;
}
