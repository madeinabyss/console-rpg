#pragma once
#include "Monster.h"
#include <random>

class Skeleton :
	public Monster
{
public:
	Skeleton(int, int);
	~Skeleton();
	virtual void fight(int& damage, int playerAttackPower, int playerArmor);

private:

};

