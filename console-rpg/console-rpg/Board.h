#pragma once
#include <vector>
#include <string>
#include "Zombie.h"
#include "Skeleton.h"


class Board
{
public:
	Board();
	~Board();
	void loadBoard();
	void displayBoard();
	void checkBoard(char condition, int& x, int& y);
	void checkBoard(std::vector<Zombie>& zombie, std::vector<Skeleton>& skeleton, int x, int y);
	int checkBoard(int x, int y);
	void modifyBoard(int x1, int y1, int x2, int y2);
	void modifyBoard(int x1, int y1);

private:
	std::vector<std::string> _board;
};

