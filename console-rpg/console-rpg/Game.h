#pragma once
#include "Board.h"
#include "Player.h"
#include "Monster.h"
#include <conio.h>
#include <string>
#include <random>
#include <time.h>
#include "Zombie.h"
#include "Skeleton.h"
#include <memory>

class Game
{
private:
	std::unique_ptr<Board> board;
	std::unique_ptr<Player> player;
	std::vector<Zombie> zombie;
	std::vector<Skeleton> skeleton;

	void playGame(std::vector<Zombie>& zombie, std::vector<Skeleton>& skeleton);
	void monsterTurn(Monster& monster);
	inline void monsterThing(std::vector<Zombie>& zombie, std::vector<Skeleton>& skeleton);
	void moveMonster(Monster& monster, char direction);
	void movePlayer(char direction);

public:
	Game();
	~Game();
	void initGame();
};

