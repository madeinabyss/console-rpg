#include "Game.h"
#include <iostream>
#include <windows.h>
#include <stdio.h>

void cls() {
	HANDLE hOut;
	COORD Position;

	hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	Position.X = 0;
	Position.Y = 0;
	SetConsoleCursorPosition(hOut, Position);
}

void clsAll() {
	HANDLE hOut;
	COORD Position;
	DWORD Written;

	hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	Position.X = 0;
	Position.Y = 0;
	FillConsoleOutputCharacter(hOut, ' ', 1000000, Position, &Written);

	SetConsoleCursorPosition(hOut, Position);
}

Game::Game() {
	board = std::make_unique<Board>();
	player = std::make_unique<Player>();
}


Game::~Game() {
}

void Game::initGame() {
	board->loadBoard();

	std::string name;
	std::cout << "Who are you? What name should I write on your grave? There was no man who could leave this dungeon alive." << std::endl;
	std::cin >> name;
	player->setName(name);

	int x = player->getPosX();
	int y = player->getPosY();

	board->checkBoard('P', x, y);
	player->setXandY(x, y);

	board->checkBoard(zombie, skeleton, 0, 0);

	playGame(zombie, skeleton);
}

void Game::monsterTurn(Monster& monster) {
	std::random_device r;

	std::default_random_engine e1(r());
	std::uniform_int_distribution<int> uniform_dist(1, 6);
	int chooseDirection = uniform_dist(e1);

	switch (chooseDirection)
	{
	case 1:
		moveMonster(monster, 'w');
		break;
	case 2:
		moveMonster(monster, 's');
		break;
	case 3:
		moveMonster(monster, 'a');
		break;
	case 4:
		moveMonster(monster, 'd');
		break;
	default:
		break;
	}

}

inline void Game::monsterThing(std::vector<Zombie>& zombie, std::vector<Skeleton>& skeleton) {
	for (auto it = zombie.begin(); it != zombie.end();) {
		if (it->_hp <= 0) {
			board->modifyBoard(it->_monsterPosX, it->_monsterPosY);
			it = zombie.erase(it);
			continue;
		}
		monsterTurn(*it);
		it++;
	}

	for (auto it = skeleton.begin(); it != skeleton.end();) {
		if (it->_hp <= 0) {
			board->modifyBoard(it->_monsterPosX, it->_monsterPosY);
			it = skeleton.erase(it);
			continue;
		}
		monsterTurn(*it);
		it++;
	}
}


void Game::moveMonster(Monster& monster, char direction) {
	int x = monster._monsterPosX;
	int y = monster._monsterPosY;

	if (board->checkBoard(x, y) != 2) return;

	int newx = monster._monsterPosX;
	int newy = monster._monsterPosY;

	if (direction == 'a') newy--;
	else if (direction == 'd') newy++;
	else if (direction == 'w') newx--;
	else if (direction == 's') newx++;

	if (board->checkBoard(newx, newy)) {
		monster.setXandY(newx, newy);
		board->modifyBoard(x, y, newx, newy);
	}

	else if (newx == player->getPosX() && newy == player->getPosY()) {
		int damage = 0;
		monster.fight(damage, player->getAttackPower(), player->getArmor());
		player->changeHP(-damage);
	}

}

void Game::movePlayer(char direction) {
	int x = player->getPosX();
	int y = player->getPosY();
	int newx = player->getPosX();
	int newy = player->getPosY();

	if (direction == 'a') newy--;
	else if (direction == 'd') newy++;
	else if (direction == 'w') newx--;
	else if (direction == 's') newx++;

	if (board->checkBoard(newx, newy) == 1) {
		player->setXandY(newx, newy);
		board->modifyBoard(x, y, newx, newy);
	}

	else if (board->checkBoard(newx, newy) == 2) {
		Monster* monster = nullptr;
		for (auto it = zombie.begin(); it != zombie.end(); it++) {
			if (it->_monsterPosX == newx && it->_monsterPosY == newy) {
				monster = &*it;
				break;
			}
		}

		if (monster == nullptr) {
			for (auto it = skeleton.begin(); it != skeleton.end(); it++) {
				if (it->_monsterPosX == newx && it->_monsterPosY == newy) {
					monster = &*it;
					break;
				}
			}
		}
		
		if (monster != nullptr) {
			int damage = 0;
			monster->fight(damage, player->getAttackPower(), player->getArmor());
			player->gainExp(300);
			player->changeHP(-damage);
		}
	}

	else if (board->checkBoard(newx, newy) == 3) {
		player->changeHP(-10000);
	}
}

template<typename T>
std::string outputToDisplay(std::string text, T data) {
	return text + " " + std::to_string(data) + "\n";
}


void Game::playGame(std::vector<Zombie>& zombie, std::vector<Skeleton>& skeleton) {
	char command;
	clsAll();
	while (player->isAlive()) {
		cls();
		board->displayBoard();
		std::cout << "\n";
		std::cout << *player; //overloaded operator
		std::cout << "Use WSAD to move. Be aware of (Z)ombies and (S)keletons and especially of (B)oss. You win if you kill them all.\n";

		std::cout << outputToDisplay("Your HP:", player->getHp());
		std::cout << outputToDisplay("Your level:", player->getLvl());
		std::cout << outputToDisplay("Your Attack Power:", player->getAttackPower());
		std::cout << outputToDisplay("Your Armor:", player->getArmor());

		command = _getch();
		switch (command) {
		case 'w':
			movePlayer('w');
			monsterThing(zombie, skeleton);
			break;
		case 's':
			movePlayer('s');
			monsterThing(zombie, skeleton);
			break;
		case 'a':
			movePlayer('a');
			monsterThing(zombie, skeleton);
			break;
		case 'd':
			movePlayer('d');
			monsterThing(zombie, skeleton);
			break;
		}

	}

	clsAll();
	std::cout << "You lost! I told you that was bad idea to go there!\n\n";

}
