#include "Zombie.h"



Zombie::Zombie(int x, int y) {
	_name = "Zombie";

	_monsterPosX = x;
	_monsterPosY = y;
	_attackPower = 15;
	_armor = 17;
	_hp = 60;
}

Zombie::Zombie() {
}


Zombie::~Zombie() {
}

void Zombie::fight(int& damage, int playerAttackPower, int playerArmor) {
	std::random_device r;

	std::default_random_engine e1(r());
	std::uniform_int_distribution<int> uniform_dist(1, 100);
	int attackChance = uniform_dist(e1);

	if (attackChance > 40) {
		damage += _attackPower - playerArmor;
	}
	_hp -= playerAttackPower - _armor;
}
